<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
  public $timestamps  = false;
  protected $rules    = array();
  protected $hidden   = ['pivot'];

  
  public function validate()
  {
    $v = \Validator::make($this->attributes, $this->rules);
    if (!$v->passes()) {
      $this->errors = $v->messages();
      
      return false;
    } 

    return true;
  }
}
