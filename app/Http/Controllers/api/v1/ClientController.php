<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request;

use App\Http\Requests\ClientRequest;
use App\Http\Controllers\Controller;
use App\Client;

class ClientController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $clients = Client::all();

    return response()->json($clients, 200);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $response = array();
    $client   = new Client($request->input());

    if($client->validate()) {
      $client->save();
      $response = response()->json($client->makeHidden(array('name', 'gender')), 200); 
    } else {
      $response = response()->json($client->errors, 400);
    }

    return $response;
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $client = Client::find($id);

    return response()->json($client, 200);      
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $response = array();
    $client   = Client::find($id);
    if($client) {
      $client->fill($request->input());

      if($client->validate()) {
        $client->save();
        $response = response()->json($client->makeHidden(array('name', 'gender')), 200); 
      } else {
        $response = response()->json($client->errors, 400);
      }
    } else {
      $response = response()->json(array('error' => true, 'message' => 'Client not found.'), 400);
    } 

    return $response;
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $response = array();
    $client   = Client::find($id);
    if($client) {
      $client->delete();
      $response = response()->json(array('error' => false, 'message' => 'Client deleted.'), 200);
    } else {
      $response = response()->json(array('error' => true, 'message' => 'Client not found.'), 400);
    }

    return $response;
  }

  public function getRentHistories($id) {
    $client = Client::with('histories')->find($id);

    if($client)
      $response = response()->json($client, 200);
    else
      $response = response()->json(array('error' => true, 'message' => 'Client not found.'), 400);

    return $response;  
  }
}
