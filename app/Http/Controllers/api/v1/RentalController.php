<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Client;


class RentalController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $results = \DB::table('rentals')
    ->join('clients', 'rentals.client_id', '=', 'clients.id')
    ->join('cars', 'rentals.car_id', '=', 'cars.id')
    ->select('clients.name', 'cars.brand', 'cars.type', 'cars.plate', 
      'rentals.date_from', 'rentals.date_to')
    ->get();

    return response()->json($results, 200);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $response = array();
    $rules    = array(
      'client_id' => 'required|row_exist:clients|count_client_car_rent:date_from',
      'car_id'    => 'required|row_exist:cars|count_rented_car:date_from',
      'date_from' => 'required|rent_duration:date_to|valid_rent_date',
      'date_to'   => 'required'
      );

    $validator = \Validator::make($request->input(), $rules);

    if($validator->fails()) {
      $messages = $validator->messages();
      $response = response()->json($messages, 400); 
    }

    $rent_id = \DB::table('rentals')->insertGetId($request->input());
    $response = response()->json(array('id' => $rent_id), 200);

    return $response;
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $results = \DB::table('rentals')
    ->join('clients', 'rentals.client_id', '=', 'clients.id')
    ->join('cars', 'rentals.car_id', '=', 'cars.id')
    ->select('clients.name', 'cars.brand', 'cars.type', 'cars.plate', 
      'rentals.date_from', 'rentals.date_to')
    ->where('rentals.id', $id)
    ->first();

    return response()->json($results, 200);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $response = array();
    $rental = \DB::table('rentals')
    ->where('id', $id)
    ->select('id')
    ->first();

    $rules    = array(
      'id'        => 'row_exist:rentals',
      'client_id' => 'required|row_exist:clients|count_client_car_rent:date_from,' . $rental->id,
      'car_id'    => 'required|row_exist:cars|count_rented_car:date_from,' . $rental->id,
      'date_from' => 'required|rent_duration:date_to|valid_rent_date',
      'date_to' => 'required'
      );

    $validator = \Validator::make($request->input(), $rules);

    if($validator->fails()) {
      $messages = $validator->messages();
      $response = response()->json($messages, 400); 
    }

    \DB::table('rentals')->where('id', $rental->id)->update($request->except('_method'));
    $response = response()->json(array('id' => $rental->id), 200);

    return $response;
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $response = array();
    
    if(\DB::table('rentals')->where('id', $id)->delete()) {
      $response = response()->json(array('error' => false, 'message' => 'Rental deleted.'), 200);
    } else {
      $response = response()->json(array('error' => true, 'message' => 'Rental not found.'), 400);
    }

    return $response;
  }

}
