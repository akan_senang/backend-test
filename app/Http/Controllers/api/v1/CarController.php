<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Car;

class CarController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $cars = Car::all();

    return response()->json($cars, 200);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $response = array();
    $car      = new Car($request->input());

    if($car->validate()) {
      $car->save();

      $response = response()->json($car->makeHidden(
        array('brand', 'type', 'year', 'color', 'plate')
        ), 200); 
    } else {
      $response = response()->json($car->errors, 400);
    }

    return $response;
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $car = Car::find($id);
    
    return response()->json($car, 200);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $response = array();
    $car      = Car::find($id);

    if($car) {
      $car->fill($request->input());
      $car->rules['plate'] = 'required|unique:cars,id,' . $car->id;

      if($car->validate()) {
        $car->save();

        $response = response()->json($car->makeHidden(
          array('brand', 'type', 'year', 'color', 'plate')
          ), 200); 
      } else {
        $response = response()->json($car->errors, 400);
      }
      
    } else {
      $response = response()->json(array('error' => true, 'message' => 'Car not found.'), 400);
    }

    return $response;
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $response = array();
    $car      = Car::find($id);
    if($car) {
      $car->delete();
      $response = response()->json(array('error' => false, 'message' => 'Car deleted.'), 200);
    } else {
      $response = response()->json(array('error' => true, 'message' => 'Car not found.'), 400);
    }

    return $response;
  }

  public function getRentHistories(Request $request, $id) {
    $rules = array(
      'month'     => 'date_format:m-Y',
      );

    $validator = \Validator::make($request->input(), $rules);

    if($validator->fails()) {
      $messages = $validator->messages();
      $response = response()->json($messages, 400); 
    } else {
      $month = $request->month;
      $car = Car::with(array('histories' => function($q) use($month){
        if($month)
          $q->whereRaw("DATE_FORMAT(date_from,'%m-%Y') = ? OR DATE_FORMAT(date_to,'%m-%Y') = ?", 
            array($month, $month));
      }))->find($id);

      if($car)
        $response = response()->json($car, 200);
      else
        $response = response()->json(array('error' => true, 'message' => 'Car not found.'), 400);
    }

    return $response;
  }

  public function getRentedCars(Request $request) {
    $rules = array(
      'date' => 'date_format:d-m-Y',
      );

    $validator = \Validator::make($request->input(), $rules);

    if($validator->fails()) {
      $messages = $validator->messages();
      $response = response()->json($messages, 400); 
    } else {
      $date = $request->date;

      $cars = Car::whereHas('histories', function($q) use($date){
        if($date)
          $q->whereRaw("STR_TO_DATE(?, '%d-%m-%Y') BETWEEN date_from AND date_to", 
            array($date));
      })->get();

      $response = response()->json(array('date' => $date, 'rented_cars' => $cars), 200);
    }

    return $response;
  }

  public function getAvailableCars(Request $request) {
    $rules = array(
      'date' => 'date_format:d-m-Y',
      );

    $validator = \Validator::make($request->input(), $rules);

    if($validator->fails()) {
      $messages = $validator->messages();
      $response = response()->json($messages, 400); 
    } else {
      $date = $request->date;

      $cars = Car::whereDoesntHave('histories', function($q) use($date){
        if($date)
          $q->whereRaw("STR_TO_DATE(?, '%d-%m-%Y') BETWEEN date_from AND date_to", 
            array($date));
      })->get();

      $response = response()->json(array('date' => $date, 'free_cars' => $cars), 200);
    }

    return $response;
  }
}
