<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot()
  {
    \Validator::extend('manufacture_year', function($attribute, $value, $parameters, $validator) {
      $dt = \Carbon\Carbon::now();

      return (int) $value <= $dt->year;
    });

    \Validator::replacer('manufacture_year', function($message, $attribute, $rule, $parameters) {
      return 'Manufacture year cannot exceed this year.';
    });

    \Validator::extend('row_exist', function($attribute, $value, $parameters, $validator) {
      $table = $parameters[0];
      $count = \DB::table($table)->where('id', $value)->count();
      return $count > 0;
    });

    \Validator::replacer('row_exist', function($message, $attribute, $rule, $parameters) {
      return 'Row not found';
    });

    \Validator::extend('count_client_car_rent', function($attribute, $value, $parameters, $validator) {

      $data       = $validator->getData();
      $date_from  = $data[$parameters[0]];

      \DB::enableQueryLog();

      $query = \DB::table('rentals')
      ->where($attribute, $value)
      ->where('date_from', '<=', $date_from)
      ->where('date_to', '>=', $date_from);

      if(array_key_exists(1, $parameters)) {
        $query->where('id', '!=', $parameters[1]);
      }

      return $query->count() == 0;
    });

    \Validator::replacer('count_client_car_rent', function($message, $attribute, $rule, $parameters) {
      return 'Client is rent another car at selected rent date';
    });

    \Validator::extend('count_rented_car', function($attribute, $value, $parameters, $validator) {

      $data       = $validator->getData();
      $date_from  = $data[$parameters[0]];

      // \DB::enableQueryLog();
      $query = \DB::table('rentals')
      ->where($attribute, $value)
      ->where('date_from', '<=', $date_from)
      ->where('date_to', '>=', $date_from);
      if(array_key_exists(1, $parameters)) {
        $query->where('id', '!=', $parameters[1]);
      }

      // print_r(\DB::getQueryLog());die();

      return $query->count() == 0;
    });

    \Validator::replacer('count_rented_car', function($message, $attribute, $rule, $parameters) {
      return 'Car is rented at selected rent date';
    });

    \Validator::extend('rent_duration', function($attribute, $value, $parameters, $validator) {

      $data       = $validator->getData();
      $date_from  = \Carbon\Carbon::createFromFormat('Y-m-d', $value);
      $date_to    = \Carbon\Carbon::createFromFormat('Y-m-d', $data[$parameters[0]]);

      $diff = $date_to->diffInDays($date_from);
      
      return $diff < 4;
    });

    \Validator::replacer('rent_duration', function($message, $attribute, $rule, $parameters) {
      return 'Rented duration max 3 days';
    });

    \Validator::extend('valid_rent_date', function($attribute, $value, $parameters, $validator) {
      $date_from  = \Carbon\Carbon::createFromFormat('Y-m-d', $value);

      return $date_from->between(\Carbon\Carbon::now()->addDay(), \Carbon\Carbon::now()->addDays(7));
    });

    \Validator::replacer('valid_rent_date', function($message, $attribute, $rule, $parameters) {
      return 'Rent date only between current day + 1 days until current date +7 days.';
    });

  }

  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
  }
}
