<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends BaseModel
{
  public $timestamps  = false;
  protected $fillable = array('brand', 'type', 'year', 'color', 'plate');
  public $rules       = array(
    'brand' => 'required',
    'type'  => 'required',
    'year'  => 'required|manufacture_year',
    'color' => 'required',
    'plate' => 'required|unique:cars'
    );

  public function histories()
  {
    return $this->belongsToMany('App\Client', 'rentals')
    ->join('clients as c', 'rentals.client_id', '=', 'c.id')
    ->select(['c.name as rent_by', 'rentals.date_from', 'rentals.date_to']);
  }

  public function getRentedCar($date = '')
  {
    return $this->belongsToMany('App\Client', 'rentals')
    ->join('clients as c', 'rentals.client_id', '=', 'c.id')
    ->select(['c.name as rent_by', 'rentals.date_from', 'rentals.date_to']);
  }  
}
